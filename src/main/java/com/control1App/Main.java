package com.control1App;

import java.io.*;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println("Результат работы методов: ");

        System.out.println("1 Method: ");
        Date date = TestHelper.getRandomDate();
        System.out.println("Случайная дата "+ date);

        System.out.println("2 Method: ");
        System.out.println("Рандомное длинное число "+ TestHelper.getRandomLong());

        System.out.println("3 Method: ");
        System.out.println("Результат перевода стрроки в дабл "+TestHelper.stringToDouble("233298"));

        System.out.println("4 Method: ");
        System.out.println("Случайная дата из метода 1 после изменения формата: \n"+ TestHelper.changeDateFormat(date));

        System.out.println("5 Method: ");
        System.out.println("HashMap из файла "+ TestHelper.getHashMap("file.txt"));

        System.out.println("6 Method: ");
        System.out.println("Три случайных последовательности символов "+ TestHelper.getRandomWords());


    }
}
