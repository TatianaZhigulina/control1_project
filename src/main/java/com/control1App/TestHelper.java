package com.control1App;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public abstract class TestHelper {
    private static Random rnd= new Random();

    private static Date privateGetRandomDate(){
        Date Curdate = new Date();
        long millies = Curdate.getTime();
        long rndMillies=rnd.nextLong(0,millies);
        Date resultDate= new Date(rndMillies);
        return resultDate;
    }

    private static long privateGetRandomLong(){
        return rnd.nextLong();
    }

    private static String privateGetRandomWords(){
        String res="";
        int symbols;
        for (int i=0; i<3;i++){
            res +=(char)(rnd.nextInt(25)+65);
            symbols= rnd.nextInt(2,8);
            for (int j=0;j<symbols;j++){
                res+=(char)(rnd.nextInt(25)+97);
            }
            res+=" ";
        }
        return res;
    }

    private static String privateChangeFormat(Date date){
        SimpleDateFormat format=new SimpleDateFormat(
                "День dd Месяц MM Год yyyy Время hh:mm");
        return format.format(date);
    }

    private static HashMap<String,String> privateGetHashMap(String path){
        HashMap<String,String> resHM = new HashMap<>();
        ArrayList<String> values = new ArrayList<>();
        try {
            File file = new File(path);
            //создаем объект FileReader для объекта File
            FileReader fr = new FileReader(file);
            //создаем BufferedReader с существующего FileReader для построчного считывания
            BufferedReader reader = new BufferedReader(fr);
            // считаем сначала первую строку
            int i = 0;
            String line = reader.readLine();

            while (line != null) {
                // считываем остальные строки в цикле
                values.add(i, line);
                i++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] keyAndValue=new String[2];
        for (String str:
             values) {
            keyAndValue=str.split(":");
            resHM.put(keyAndValue[0],keyAndValue[1]);
        }
        return resHM;
    }


    private static double privateStringToDouble(String str){
        try{
            double res=Double.valueOf(str);
            return res;
        }catch (Exception e){
            return Double.POSITIVE_INFINITY;
        }
    }


    //1
    public static Date getRandomDate(){
        return privateGetRandomDate();
    }

    //2
    public static long getRandomLong(){
        return privateGetRandomLong();
    }

    //3
    public static String getRandomWords(){
        return privateGetRandomWords();
    }

    //4
    public static String changeDateFormat(Date date){
        return privateChangeFormat(date);
    }

    //5
    public static HashMap<String,String> getHashMap(String fileName){
        return privateGetHashMap(fileName);
    }

    //6
    public static double stringToDouble(String str){
        return privateStringToDouble(str);
    }

}
