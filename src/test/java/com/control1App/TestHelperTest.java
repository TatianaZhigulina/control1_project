package com.control1App;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class TestHelperTest {



    @Test
    void getRandomDate() {
        long milsec = TestHelper.getRandomDate().getTime();
        Date date = new Date();
        if ((milsec<0) || (milsec>=date.getTime())){
            assert(false);
        }

    }

    @Test
    void getRandomLong() {
        if (Long.toString(TestHelper.getRandomLong()).contains(".")){
            assert(false);
        }
    }

    @Test
    void getRandomWords() {
        String []splitArray = TestHelper.getRandomWords().split(" ");
        assertEquals(splitArray.length-1,2);
    }

    @Test
    void changeDateFormat() {
        //День 26 Месяц 09 Год 2021 Время 01:39
         Date date = new Date();
         if (!TestHelper.changeDateFormat(date)
                 .matches("День [0-3][0-9] Месяц [0-1][0-9] Год [12][0-9]{3} Время [0-9][0-9]:[0-5][0-9]")){
             assert (false);
         }
    }

    @Test
    void getHashMap() {
        HashMap<String,String> hashM = new HashMap<>();
        hashM = TestHelper.getHashMap("file.txt");
        assertEquals(hashM.get("data"),"data");
    }

    @Test
    void stringToDouble() {
       assertEquals(TestHelper.stringToDouble("123456"),123456);
    }
}